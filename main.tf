terraform {
  required_providers {
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

provider "random" {
  interpreter = ["/bin/sh", "-c"]
  enable_parallelism = false
  command = "free -m"
}